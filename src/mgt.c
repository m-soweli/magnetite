/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <glib/gi18n.h>

#include "mgt-application.h"
#include "mgt-config.h"

int
main(int   argc,
     char *argv[])
{
  g_autoptr (MgtApplication) app = NULL;

  bindtextdomain (MGT_CONFIG_I18N, MGT_CONFIG_I18N_DIR);
  bind_textdomain_codeset (MGT_CONFIG_I18N, "UTF-8");
  textdomain (MGT_CONFIG_I18N);

  app = mgt_application_new ();

  return g_application_run (G_APPLICATION (app), argc, argv);
}
