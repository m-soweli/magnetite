/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "mgt-application.h"
#include "mgt-application-window.h"
#include "mgt-config.h"
#include "mgt-platform.h"
#include "mgt-platform-device.h"

struct _MgtApplication
{
  AdwApplication parent;

  PeasEngine       *peas_engine;
  PeasExtensionSet *peas_extensions_platform;

  GtkWindow *window;
};

G_DEFINE_TYPE (MgtApplication, mgt_application, ADW_TYPE_APPLICATION)

static void
mgt_application_activate (GApplication *app)
{
  MgtApplication *self = MGT_APPLICATION (app);

  if (self->window == NULL)
      self->window = GTK_WINDOW (mgt_application_window_new (self));

  gtk_window_present (GTK_WINDOW (self->window));
}

static void
mgt_application_startup (GApplication *app)
{
  MgtApplication *self = MGT_APPLICATION (app);

  self->peas_engine = peas_engine_new ();
  self->peas_extensions_platform = peas_extension_set_new (self->peas_engine, MGT_TYPE_PLATFORM, NULL);

  peas_engine_add_search_path (self->peas_engine,
                               "resource://org/tachibana_labs/Magnetite/plugins",
                               "resource://org/tachibana_labs/Magnetite/plugins");

  for (int i = 0; i < g_list_model_get_n_items (G_LIST_MODEL (self->peas_engine)); i++)
    {
      g_autoptr (PeasPluginInfo) peas_info = g_list_model_get_item (G_LIST_MODEL (self->peas_engine), i);
      peas_engine_load_plugin (self->peas_engine, peas_info);
    }

  G_APPLICATION_CLASS (mgt_application_parent_class)->startup (app);
}

static void
mgt_application_action_about (GSimpleAction  *action,
                              GVariant       *action_param,
                              gpointer        aux)
{
  MgtApplication *self = MGT_APPLICATION (aux);

  adw_show_about_dialog (GTK_WIDGET (self->window),
                         "application-name", MGT_CONFIG_NAME,
                         "application-icon", "application-x-executable",
                         "version", MGT_CONFIG_VERSION,
                         "issue-url", "https://gitlab.gnome.org/m-soweli/magnetite/issues",
                         "developer-name", "Mia Soweli",
                         "developers", (const char * const []) {
                           "Mia Soweli",
                           NULL,
                         },
                         "designers", (const char * const []) {
                           "Mia Soweli",
                           NULL,
                         },
                         "license-type", GTK_LICENSE_GPL_3_0_ONLY,
                         "copyright", "© 2024 Mia Soweli",
                         NULL);
}

static void
mgt_application_class_init (MgtApplicationClass *class)
{
  GApplicationClass *class_app = G_APPLICATION_CLASS (class);

  class_app->activate = mgt_application_activate;
  class_app->startup = mgt_application_startup;
}

static void
mgt_application_init (MgtApplication *self)
{
  static const GActionEntry actions[] = {
      { "about", mgt_application_action_about },
  };

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   actions, G_N_ELEMENTS (actions),
                                   self);
}

MgtApplication *
mgt_application_new (void)
{
  return g_object_new (MGT_TYPE_APPLICATION,
                       "application-id", MGT_CONFIG_ID,
                       NULL);
}
