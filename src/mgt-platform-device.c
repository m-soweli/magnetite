/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "mgt-platform.h"
#include "mgt-platform-device.h"

G_DEFINE_INTERFACE (MgtPlatformDevice, mgt_platform_device, G_TYPE_OBJECT)

static void
mgt_platform_device_default_init (MgtPlatformDeviceInterface *class)
{
  g_object_interface_install_property (class,
    g_param_spec_uint (
                       "id-vendor",
                       "",
                       "",
                       0x0000,
                       0xffff,
                       0xffff,
                       G_PARAM_READABLE));

  g_object_interface_install_property (class,
    g_param_spec_uint (
                       "id-product",
                       "",
                       "",
                       0x0000,
                       0xffff,
                       0xffff,
                       G_PARAM_READABLE));
}

guint16
mgt_platform_device_get_id_vendor (MgtPlatformDevice *self)
{
  g_return_val_if_fail (MGT_IS_PLATFORM_DEVICE (self), 0xffff);

  return MGT_PLATFORM_DEVICE_GET_IFACE (self)->get_id_vendor(self);
}

guint16
mgt_platform_device_get_id_product (MgtPlatformDevice *self)
{
  g_return_val_if_fail (MGT_IS_PLATFORM_DEVICE (self), 0xffff);

  return MGT_PLATFORM_DEVICE_GET_IFACE (self)->get_id_product(self);
}
