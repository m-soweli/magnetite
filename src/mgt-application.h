/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <adwaita.h>
#include <glib.h>
#include <glib-object.h>
#include <libpeas.h>

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE (MgtApplication, mgt_application, MGT, APPLICATION, AdwApplication)

#define MGT_TYPE_APPLICATION (mgt_application_get_type())

MgtApplication * mgt_application_new (void);

G_END_DECLS
