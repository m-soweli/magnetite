/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

G_DECLARE_INTERFACE (MgtPlatformDevice, mgt_platform_device, MGT, PLATFORM_DEVICE, GObject)

#define MGT_TYPE_PLATFORM_DEVICE (mgt_platform_device_get_type())

struct _MgtPlatformDeviceInterface
{
  GObject parent;

  guint16 (*get_id_vendor)  (MgtPlatformDevice *);
  guint16 (*get_id_product) (MgtPlatformDevice *);
};

guint16 mgt_platform_device_get_id_vendor  (MgtPlatformDevice *);
guint16 mgt_platform_device_get_id_product (MgtPlatformDevice *);

G_END_DECLS
