/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "mgt-platform-device.h"

G_BEGIN_DECLS

G_DECLARE_INTERFACE (MgtPlatform, mgt_platform, MGT, PLATFORM, GObject)

#define MGT_TYPE_PLATFORM (mgt_platform_get_type())

struct _MgtPlatformInterface
{
  GObject parent;

  GListModel * (*get_devices)(MgtPlatform *);
};

void mgt_platform_emit_device_added   (MgtPlatform *, MgtPlatformDevice *);
void mgt_platform_emit_device_removed (MgtPlatform *, MgtPlatformDevice *);

GListModel * mgt_platform_get_devices (MgtPlatform *);

G_END_DECLS
