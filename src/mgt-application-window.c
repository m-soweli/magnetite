/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "mgt-application.h"
#include "mgt-application-window.h"

struct _MgtApplicationWindow
{
  AdwApplicationWindow parent;

  AdwNavigationView *navigation;
  AdwNavigationPage *navigation_home;
};

G_DEFINE_FINAL_TYPE (MgtApplicationWindow, mgt_application_window, ADW_TYPE_APPLICATION_WINDOW)

static void
mgt_application_window_class_init (MgtApplicationWindowClass *class)
{
  GtkWidgetClass *class_widget = GTK_WIDGET_CLASS (class);

  gtk_widget_class_set_template_from_resource (class_widget, "/org/tachibana_labs/Magnetite/mgt-application-window.ui");

  gtk_widget_class_bind_template_child (class_widget, MgtApplicationWindow, navigation);
  gtk_widget_class_bind_template_child (class_widget, MgtApplicationWindow, navigation_home);
}

static void
mgt_application_window_init (MgtApplicationWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

MgtApplicationWindow *
mgt_application_window_new (MgtApplication *app)
{
  return g_object_new (MGT_TYPE_APPLICATION_WINDOW,
                       "application", app,
                       NULL);
}
