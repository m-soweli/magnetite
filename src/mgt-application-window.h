/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include "mgt-application.h"

#include <adwaita.h>

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE (MgtApplicationWindow, mgt_application_window, MGT, APPLICATION_WINDOW, AdwApplicationWindow)

#define MGT_TYPE_APPLICATION_WINDOW (mgt_application_window_get_type())

MgtApplicationWindow * mgt_application_window_new (MgtApplication *);

G_END_DECLS
