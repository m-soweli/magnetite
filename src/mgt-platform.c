/*
 * Copyright © Mia Soweli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "mgt-platform.h"
#include "mgt-platform-device.h"

G_DEFINE_INTERFACE (MgtPlatform, mgt_platform, G_TYPE_OBJECT)

static guint mgt_platform_signal_device_added;
static guint mgt_platform_signal_device_removed;

static void
mgt_platform_default_init (MgtPlatformInterface *class)
{
    mgt_platform_signal_device_added = g_signal_new ("device-added",
                                                     MGT_TYPE_PLATFORM,
                                                     G_SIGNAL_RUN_LAST,
                                                     0,
                                                     NULL, NULL, NULL,
                                                     G_TYPE_NONE,
                                                     1,
                                                     MGT_TYPE_PLATFORM_DEVICE);

  mgt_platform_signal_device_removed = g_signal_new ("device-removed",
                                                     MGT_TYPE_PLATFORM,
                                                     G_SIGNAL_RUN_LAST,
                                                     0,
                                                     NULL, NULL, NULL,
                                                     G_TYPE_NONE,
                                                     1,
                                                     MGT_TYPE_PLATFORM_DEVICE);
}

void
mgt_platform_emit_device_added (MgtPlatform       *self,
                                MgtPlatformDevice *device)
{
  g_return_if_fail (MGT_IS_PLATFORM (self));
  g_return_if_fail (MGT_IS_PLATFORM_DEVICE (device));

  g_signal_emit (self, mgt_platform_signal_device_added, 0, device);
}

void
mgt_platform_emit_device_removed (MgtPlatform       *self,
                                  MgtPlatformDevice *device)
{
  g_return_if_fail (MGT_IS_PLATFORM (self));
  g_return_if_fail (MGT_IS_PLATFORM_DEVICE (device));

  g_signal_emit (self, mgt_platform_signal_device_removed, 0, device);
}

GListModel *
mgt_platform_get_devices (MgtPlatform *self)
{
  g_return_val_if_fail (MGT_IS_PLATFORM (self), NULL);

  return MGT_PLATFORM_GET_IFACE (self)->get_devices (self);
}
